import axios from 'axios';

const getCategory = async() => {
    try{
        return await axios.get(`https://opentdb.com/api_category.php`);
    } catch (error) {
        console.log(error);
    }
}

const getQuestion = async (amount, category) => {
    try{
        return await axios.get(`https://opentdb.com/api.php?amount=${amount}&category=${category}&difficulty=medium&type=multiple`);
    } catch (error) {
        console.log(error);
    }
}

const api = {};
api.getCategory = getCategory;
api.getQuestion = getQuestion;
export default api;